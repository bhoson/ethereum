pragma solidity ^0.5.1;


contract Ballot 
{
    
    struct Voter {
        uint weight; 
        bool voted;  
        address delegate;
        uint vote;
    }


    struct Proposal {
        bytes32 name;
        uint voteCount;
    }

    address public chairperson;

    
    mapping(address => Voter) public voters;

    
    Proposal[] public proposals;



    constructor(bytes32[] memory proposalNames, address cperson) public {
        chairperson = cperson;
        voters[chairperson].weight = 1;
        
        /// logging the address of chairperson
        log1(bytes32("Chairperson: "),bytes32(uint256(chairperson) << 96));
        
        for (uint i = 0; i < proposalNames.length; i++) 
        {
            
            proposals.push(Proposal({
                name: proposalNames[i],
                voteCount: 0
            }));
        }
        /// logging the address
        log1(bytes32("Proposal Count: "),bytes32(proposalNames.length));
    }


    
    function giveRightToVote(address voter, address sender) public 
    {
        require(
            sender == chairperson,
            "Only chairperson can give right to vote."
        );
        require(
            !voters[voter].voted,
            "The voter already voted."
        );
        /// logging the address
        log1(bytes32("Giving rights to: "),bytes32(uint256(voter) << 96));
        
        require(voters[voter].weight == 0,"Already rights given!");
        voters[voter].weight = 1;
        voters[voter].voted = false;
    }


    function delegate(address delegator, address to) public 
    {
        Voter memory sender = voters[delegator];
        require(!sender.voted, "You already voted.");

        require(to != delegator, "Self-delegation is disallowed.");
        
        voters[delegator].voted = true;
        voters[delegator].delegate = to;
        
        Voter storage delegate_ = voters[to];
        if (delegate_.voted) {
            
            proposals[delegate_.vote].voteCount += sender.weight;
        } else {
        
            delegate_.weight += sender.weight;
        }
    }


    function vote(uint proposal, address voter) payable public 
    {
        /// logging the voter
        log1(bytes32("Voter Addr: "),bytes32(uint256(voter) << 96));
        
        require(voters[voter].weight == 1,"No voting right!");
        require(!voters[voter].voted, "Already voted.");
        
        Voter memory sender = voters[voter];
        
        
        voters[voter].voted = true;
        voters[voter].vote = proposal;
        proposals[proposal].voteCount += sender.weight;
    }



    function winningProposal() public view
            returns (uint winningProposal_)
    {
        uint winningVoteCount = 0;
        for (uint p = 0; p < proposals.length; p++) {
            if (proposals[p].voteCount > winningVoteCount) {
                winningVoteCount = proposals[p].voteCount;
                winningProposal_ = p;
            }
        }
    }


    
    function winnerName() public view
            returns (bytes32 winnerName_)
    {
        winnerName_ = proposals[winningProposal()].name;

    }
}

