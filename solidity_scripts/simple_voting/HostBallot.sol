pragma solidity >=0.4.22 <0.6.0;
import "./Ballot.sol";

contract HostBallot {
   
    Ballot ballotToTest;
    bytes32[] candidateProposals;

        
                
    function beforeAll () public 
    {
        candidateProposals.push("Person A");
        candidateProposals.push("Person B");
        candidateProposals.push("Person C");
        ballotToTest = new Ballot(candidateProposals, msg.sender);
        
        ballotToTest.giveRightToVote(0x14723A09ACff6D2A60DcdF7aA4AFf308FDDC160C,msg.sender);
        ballotToTest.giveRightToVote(0x4B0897b0513fdC7C541B6d9D7E929C4e5364D2dB,msg.sender);
        ballotToTest.giveRightToVote(0x583031D1113aD414F02576BD6afaBfb302140225,msg.sender);
        ballotToTest.giveRightToVote(0xdD870fA1b7C4700F2BD7f44238821C26f7392148,msg.sender);
 
    }
    
    function delegatetion(address delegateTo) public
    {
        ballotToTest.delegate(msg.sender, delegateTo);
    }

    function castVote(uint proposal) public
    {
        ballotToTest.vote(proposal,msg.sender);
    }
    
    function winner() public view 
        returns (bytes32 winner)
    {
        winner = ballotToTest.winnerName();
    }
}


