const assert = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');
const { interface, bytecode } = require('../index');
const async = require('asyncawait/async');
const await = require('asyncawait/await');

const w3 = new Web3(ganache.provider());


let accounts;
let simpleInbox;



const doThis = async(function (){

  // Fetch accounts from ganache
  accounts = await (w3.eth.getAccounts());

  // Use one of the accounts to deploy the contract
  simpleInbox = await (new w3.eth.Contract(JSON.parse(interface))
  .deploy({ data: bytecode, arguments: ['dotdash'] })
  .send({ from: accounts[0], gas: '1000000', gasPrice: '3000'}));

//  simpleInbox = await (new w3.eth.Contract(JSON.parse(interface))
//  .deploy({ data: bytecode, arguments: ['dot-dash'] })
//  .send({ from: accounts[0], gas: '1000000', gasPrice: '3000'}));



});

beforeEach(doThis);


describe('Testing for SimpleInbox Contract',() => {

  it('deploys a contract',() => {
   assert.ok(simpleInbox);
  });


  it('contains default message', async(() => {
    const message = await (simpleInbox.methods.message().call());
    assert.equal(message, 'dotdash');
  }));
});


