const path = require('path');
const fs = require ('fs');
const solc = require('solc');



const projectPath = path.resolve(__dirname,'contracts','SimpleInbox.sol');
const src = fs.readFileSync(projectPath,'utf8');

//console.log(solc.compile(src,1));

module.exports = solc.compile(src,1).contracts[':SimpleInbox'];


