pragma solidity ^0.4.19;

contract SimpleInbox{
    string public message;

    function setMessage(string memory initialMessage) public {
        message = initialMessage;
    }

    function getMessage() public view returns (string memory) {
        return  message;
    }
}




