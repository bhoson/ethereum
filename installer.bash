#!/bin/bash

#---------------------------------------------#
#  Installing and setting up an ethereum node #
#---------------------------------------------#

PWD=$(pwd)
DATA_DIR=$PWD"/test_net_data_dir"


printf "\n\n[Adding repository to install golang]\n\n"
rpm --import https://mirror.go-repo.io/centos/RPM-GPG-KEY-GO-REPO
curl -s https://mirror.go-repo.io/centos/go-repo.repo | tee /etc/yum.repos.d/go-repo.repo



printf "\n\n[Installing packages]\n\n"
yum install vim wget golang gcc -y -q -e 0

printf "\n\n[Cloning ethereum sources]"
git clone https://github.com/ethereum/go-ethereum
cd go-ethereum
make geth


PWD=$(pwd)
ETH_PATH=$PWD"/build/bin/"
echo "export PATH=$PATH:$ETH_PATH" >> /etc/bashrc
export PATH=$PATH:$ETH_PATH
mkdir $DATA_DIR


printf "\n\n[Install GCC]\n\n"
yum update openssl -y 
yum -y install gcc-c++
printf "\n\n[Install NodeJS]\n\n"
yum install -y nodejs

printf "\n\n[Install CMAKE 3.13.1 from source]\n\n"
wget https://cmake.org/files/LatestRelease/cmake-3.13.1.tar.gz
tar -zxvf cmake-3.13.1.tar.gz
cd cmake-3.13.1
sudo ./bootstrap --prefix=/usr/local
sudo make
sudo make install
echo "export PATH=$PATH:/usr/local" >> ~./bash_profile


cd ~
mkdir sample_solidity_project
cd sample_solidity_project
printf "\n\n[Install Solidity compiler]\n\n"
npm install solc mocha ganache-cli web3@1.0.0-beta.26




# ----------------------------------------------------------#
# Installing packages for interacting with web3 from python #
# ----------------------------------------------------------#

printf "\n\n[Install repo for python3.6]\n\n"
yum install -y https://centos7.iuscommunity.org/ius-release.rpm

printf "\n\n[Install python3.6]\n\n"
yum install -y python36u python36u-libs python36u-devel python36u-pip

echo "alias python='/bin/python3.6'" >> /etc/bashrc
echo "alias pip='/bin/pip3.6'" >> /etc/bashrc
alias python='/bin/python3.6'
alias pip='/bin/pip3.6'

printf "\n\n[Install web3]\n\n"
pip install web3
printf "\n\n[Install pysolc]\n\n"
pip install py-solc
python -m solc.install v0.4.25
SOLC_BIN_PATH=$(find / -name solc | grep "solc-v0.4.25")
echo "export SOLC_BINARY=$SOLC_BIN_PATH" >> /etc/bashrc
export SOLC_BINARY=$SOLC_BIN_PATH



#---------------------------#
# Steps after installation  #
#---------------------------#

# Step 1) geth --datadir $DATA_DIR init $PWD+"/"+genesis.json

# Step 2) geth --identity "ETH_node_1" --nodiscover --maxpeers 2 --rpc --rpcapi "admin,db,eth,net,web3" --rpcaddr "10.10.4.139" --rpcport "8080" --datadir "/root/ethereum/test_net_data_dir" --port "30303" --networkid 666 init genesis.json 

# Step 3) geth --identity "ETH_node_1" --nodiscover --maxpeers 2 --rpc --rpcapi "admin,db,eth,net,web3" --rpcaddr "10.10.4.139" --rpcport "8080" --datadir "/root/ethereum/test_net_data_dir" --port "30303" --networkid 666 console



# Starting a node [Discovery ON]
#    geth --identity "ETH_node_1" --maxpeers 2 --rpc --rpcapi "admin,personal,db,eth,net,web3,shh,debug" --rpcaddr "10.10.0.239" --rpcport "8080" --ipcdisable --datadir "/root/ethereum/test_net_data_dir" --nat extip:10.10.0.239 --port "30303" --networkid 666 console

# Starting a node [Discovery OFF]
#   geth --identity "ETH_node_1" --nodiscover --maxpeers 2 --rpc --rpcapi "admin,personal,db,eth,net,web3,shh,debug" --rpcaddr "10.10.0.239" --rpcport "8080" --ipcdisable --datadir "/root/ethereum/test_net_data_dir" --nat extip:10.10.0.239 --port "30303" --networkid 666 console


